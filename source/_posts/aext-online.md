---
title: AEXT Online
date: 2020-05-01 18:41:24
tags:
---

  AEXT is online today on Labor Day.

  The features provided currently:

- Base coin is ECC (Eleccoin).
- BTC/ECC, LTC/ECC trade pair.
- Online hot wallet.
- Web user interface for Mobile.
- Affiliate, earn commissions by adding your affiliate tag to any AEXT URL.
- API for trade robot.
- Coin Listing service, 0.2 BTC each year.

